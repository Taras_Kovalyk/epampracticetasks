﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            var firstRect = new Rectangle(1, 10, 10, 1);
            Console.WriteLine($"First rectangle coordinates: x1={firstRect.X1};y1={firstRect.Y1};x2={firstRect.X2};y2={firstRect.Y2}");
            var secondRect = new Rectangle(5, 20, 20, 0);
            Console.WriteLine($"Second rectangle coordinates: x1={secondRect.X1};y1={secondRect.Y1};x2={secondRect.X2};y2={secondRect.Y2}");
            secondRect.Y2 = 5;
            // Move rectangle up by 1
            secondRect.Move(0, 1);
            //  Scale rectangle  vertically by 1.1
            secondRect.Scale(1, 1.1);
            Console.WriteLine($"Second rectangle coordinates after moving and changing size: x1={secondRect.X1};y1={secondRect.Y1};x2={secondRect.X2};y2={secondRect.Y2}");
            var wrappingRect = firstRect.GetWrappingRectangle(secondRect);
            Console.WriteLine($"Wrapped rectangle coordinates: x1={wrappingRect.X1};y1={wrappingRect.Y1};x2={wrappingRect.X2};y2={wrappingRect.Y2}");
            var intersectRect = firstRect.GetIntersectionRectangle(secondRect);
            Console.WriteLine($"Intersect rectangle coordinates: x1={intersectRect.X1};y1={intersectRect.Y1};x2={intersectRect.X2};y2={intersectRect.Y2}");
            Console.ReadKey();
        }
    }
}
