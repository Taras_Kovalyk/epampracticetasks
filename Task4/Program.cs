﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            var firstMatrix = new Matrix(new double[,] { { 1, 2 }, { 3, 4 } });
            Console.WriteLine("First matrix:");
            Print(firstMatrix);
            var secondMatrix = new Matrix(new double[,] { { 5, 6 }, { 7, 8 } });
            Console.WriteLine("Second matrix:");
            Print(secondMatrix);
            var resultMatrix = firstMatrix.Add(secondMatrix);
            Console.WriteLine("Addition result:");
            Print(resultMatrix);
            Console.WriteLine("Subtraction result:");
            resultMatrix = firstMatrix.Subtract(secondMatrix);
            Print(resultMatrix);
            Console.WriteLine("Multiplication result:");
            resultMatrix = firstMatrix.Multiply(secondMatrix);
            Print(resultMatrix);
            var thirdMatrix = new Matrix(new double[,] { { 1, 2,1,0 }, { 0,2,2,3 },{ 1,4,3,3},{ 3,5,6,2} });
            Console.WriteLine("Third matrix:");
            Print(thirdMatrix);
            Console.WriteLine($"Third matrix minor of element M[1,1]: {thirdMatrix.GetMinor(1,1)}");
            Console.WriteLine($"Third matrix minor of order 3 on rows 0,1,2, columns 0,2,3: {thirdMatrix.GetMinor(new int[] { 0,1, 2 }, new int[] { 0, 2, 3 })}");
            Console.ReadKey();
        }
        public static void Print(Matrix matrix)
        {
            for (int i = 0; i < matrix.RowCount; i++)
            {
                for (int j = 0; j < matrix.ColumnCount; j++)
                {
                    Console.Write($"{matrix[i, j]} ");
                    if (j == matrix.ColumnCount - 1)
                    {
                        Console.WriteLine();
                    }
                }
            }
        }
    }
}
