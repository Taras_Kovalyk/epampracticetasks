﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    /// <summary>
    /// Represents zero based matrix, provides methods to add, subtract, multiply matrixes and calculate minors
    /// </summary>
    class Matrix
    {
        private double[,] _array;

        public double this [int i,int j]
        {
            get
            {
                return _array[i, j];
            }
            set
            {
                _array[i, j] = value;
            }
        }
        public double RowCount
        {
            get
            {
                return _array.GetLength(0);
            }
        }
        public double ColumnCount
        {
            get
            {
                return _array.GetLength(0);
            }
        }      

        public Matrix(double[,] array)
        {
            _array = array;
        }
        public Matrix Add(Matrix otherMatrix)
        {
            var resArr = new double[_array.GetLength(0), _array.GetLength(1)];
            for (int i = 0; i < _array.GetLength(0); i++)
            {
                for (int j = 0; j < _array.GetLength(1); j++)
                {
                    resArr[i, j] = _array[i, j] + otherMatrix._array[i, j];
                }
            }
            return new Matrix(resArr);
        }
        public Matrix Subtract(Matrix otherMatrix)
        {
            var resArr = new double[_array.GetLength(0), _array.GetLength(1)];
            for (int i = 0; i < _array.GetLength(0); i++)
            {
                for (int j = 0; j < _array.GetLength(1); j++)
                {
                    resArr[i, j] = _array[i, j] - otherMatrix._array[i, j];
                }
            }
            return new Matrix(resArr);
        }
        public Matrix Multiply(Matrix otherMatrix)
        {
            if (_array.GetLength(1) != otherMatrix._array.GetLength(0))
            {
                throw new InvalidOperationException("Matrixes can't be multiplied");
            }
            var resArr = new double[_array.GetLength(0), _array.GetLength(1)];
            for (int i = 0; i < _array.GetLength(0); i++)
            {
                for (int j = 0; j < _array.GetLength(1); j++)
                {
                    double res = 0;
                    for (int k = 0; k < _array.GetLength(1); k++)
                        res += _array[i, k] * otherMatrix._array[k, j];
                    resArr[i, j] = res;
                }
            }
            return new Matrix(resArr);
        }
        /// <summary>
        /// Calculates minor by indexes of row and column to be removed
        /// </summary>        
        public double GetMinor(int rowToBeRemoved,int columnToBeRemoved)
        {
            if (_array.GetLength(0) != _array.GetLength(1))
            {
                throw new InvalidOperationException("Matrix is not square");
            }
            var arr = new double[_array.GetLength(0)-1,_array.GetLength(0)-1];
            for(int k=0;k<arr.GetLength(0);k++)
            {
                for (int l = 0; l < arr.GetLength(1); l++)
                {
                    int i = k, j = l;
                    if (k >= rowToBeRemoved)
                    {
                        i++;
                    }
                    if (l >= columnToBeRemoved)
                    {
                        j++;
                    }
                    arr[k, l] = _array[i, j];
                }
            }
            return Determinant(arr);
        }
        /// <summary>
        /// Calculates minor by indexes of rows and columns to be selected
        /// </summary>        
        public double GetMinor(int[] rowsToBeSelected, int[] columnsToBeRemoved)
        {
            var arr = new double[rowsToBeSelected.Length, columnsToBeRemoved.Length];
            rowsToBeSelected = rowsToBeSelected.OrderBy(x => x).ToArray(); ;
            columnsToBeRemoved = columnsToBeRemoved.OrderBy(x => x).ToArray(); ;
            for (int k = 0; k < arr.GetLength(0); k++)
            {
                for (int l = 0; l < arr.GetLength(1); l++)
                {
                    arr[k, l] = _array[rowsToBeSelected[k], rowsToBeSelected[l]];
                }
            }
            if (arr.GetLength(0) != arr.GetLength(1))
            {
                throw new InvalidOperationException("Matrix is not square.");
            }
            return Determinant(arr);
        }
        private double Determinant(double[,] arr)
        {
            if (arr.GetLength(0) != arr.GetLength(1))
            {
                throw new InvalidOperationException("Matrix is not square");
            }
            if (arr.GetLength(0) == 0)
            {
                return 1;
            }
            if (arr.GetLength(0) == 1)
            {
                return arr[0, 0];
            }
            var arr11 = new double[arr.GetLength(0) - 1, arr.GetLength(0) - 1];
            for (int i = 0; i < arr.GetLength(0) - 1; i++)
            {
                for (int j = 0; j < arr.GetLength(0) - 1; j++)
                {
                    arr11[i, j] = arr[i + 1, j + 1];
                }
            }
            var arrkk = new double[arr.GetLength(0) - 1, arr.GetLength(0) - 1];
            for (int i = 0; i < arr.GetLength(0) - 1; i++)
            {
                for (int j = 0; j < arr.GetLength(0) - 1; j++)
                {
                    arrkk[i, j] = arr[i, j];
                }
            }
            var arr1k = new double[arr.GetLength(0) - 1, arr.GetLength(0) - 1];
            for (int i = 0; i < arr.GetLength(0) - 1; i++)
            {
                for (int j = 0; j < arr.GetLength(0) - 1; j++)
                {
                    arr1k[i, j] = arr[i + 1, j];
                }
            }
            var arrk1 = new double[arr.GetLength(0) - 1, arr.GetLength(0) - 1];
            for (int i = 0; i < arr.GetLength(0) - 1; i++)
            {
                for (int j = 0; j < arr.GetLength(0) - 1; j++)
                {
                    arrk1[i, j] = arr[i, j + 1];
                }
            }
            var arr11kk = new double[arr.GetLength(0) - 2, arr.GetLength(0) - 2];
            for (int i = 0; i < arr.GetLength(0) - 2; i++)
            {
                for (int j = 0; j < arr.GetLength(0) - 2; j++)
                {
                    arr11kk[i, j] = arr[i + 1, j + 1];
                }
            }
            return ((Determinant(arr11) * Determinant(arrkk) - Determinant(arr1k) * Determinant(arrk1)) / Determinant(arr11kk));
        }
    }
}
